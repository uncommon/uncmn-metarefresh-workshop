This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Project setup
npm install

## Run the app in development mode 
npm start 


Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.