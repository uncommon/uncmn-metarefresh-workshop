// import firebase from 'firebase'

// import 'firebase/database'

import firebase from 'firebase/app'
import 'firebase/firestore'

const config = {
  apiKey: 'AIzaSyBLxprLoV62nn3nt-xi6dlb8AeSm8zfrKM',
  authDomain: 'uncmn-metarefresh-workshop.firebaseapp.com',
  databaseURL: 'https://uncmn-metarefresh-workshop.firebaseio.com',
  projectId: 'uncmn-metarefresh-workshop',
  storageBucket: 'uncmn-metarefresh-workshop.appspot.com',
  messagingSenderId: '85786544316'
}

const firebaseInstance = firebase.initializeApp(config)
// const firebaseDefaultDatabase = firebaseInstance.database()

const firebaseFirestore = firebaseInstance.firestore()

firebaseFirestore.settings({ timestampsInSnapshots: true })

export default firebaseFirestore
