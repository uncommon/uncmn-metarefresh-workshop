export const OPEN_LOGIN_MODAL = 'OPEN_LOGIN_MODAL'
export const CLOSE_LOGIN_MODAL = 'CLOSE_LOGIN_MODAL'
export const OPEN_NSFW_MODAL = 'OPEN_NSFW_MODAL'
export const CLOSE_NSFW_MODAL = 'CLOSE_NSFW_MODAL'

export function openLoginModal() {
  return {
    type: OPEN_LOGIN_MODAL
  }
}

export function closeLoginModal() {
  return {
    type: CLOSE_LOGIN_MODAL
  }
}

export function openNsfwModal() {
  return {
    type: OPEN_NSFW_MODAL
  }
}

export function closeNsfwModal() {
  return {
    type: CLOSE_NSFW_MODAL
  }
}
