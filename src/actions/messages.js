export const REQUEST_MESSAGES = 'REQUEST_MESSAGES'
export const RECEIVE_MESSAGES = 'RECEIVE_MESSAGES'

export function receiveMessages(messages) {
  return {
    type: RECEIVE_MESSAGES,
    messages
  }
}
