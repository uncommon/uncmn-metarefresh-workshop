import { combineReducers } from 'redux'

import me from './me'
import modals from './modals'
import users from './users'
import messages from './messages'

export default combineReducers({
  me,
  modals,
  users,
  messages
})
