import {
  OPEN_LOGIN_MODAL,
  CLOSE_LOGIN_MODAL,
  OPEN_NSFW_MODAL,
  CLOSE_NSFW_MODAL
} from '../actions/modals'

function modalsReducer(
  state = {
    isLoginModalOpen: true,
    isNsfwModalOpen: false
  },
  action
) {
  switch (action.type) {
    case OPEN_LOGIN_MODAL: {
      return {
        ...state,
        isLoginModalOpen: true
      }
    }
    case CLOSE_LOGIN_MODAL: {
      return {
        ...state,
        isLoginModalOpen: false
      }
    }
    case OPEN_NSFW_MODAL: {
      return {
        ...state,
        isNsfwModalOpen: true
      }
    }
    case CLOSE_NSFW_MODAL: {
      return {
        ...state,
        isNsfwModalOpen: false
      }
    }
    default: {
      return state
    }
  }
}

export default modalsReducer
