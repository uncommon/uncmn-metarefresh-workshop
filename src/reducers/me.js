import { LOGIN } from '../actions/me'

function meReducer(state = null, action) {
  switch (action.type) {
    case LOGIN: {
      return {
        ...state,
        ...action.user
      }
    }
    default: {
      return state
    }
  }
}

export default meReducer
