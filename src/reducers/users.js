import { REQUEST_USERS, RECEIVE_USERS } from '../actions/users'

function usersReducer(state = [], action) {
  switch (action.type) {
    case REQUEST_USERS: {
      return state
    }
    case RECEIVE_USERS: {
      return [...state, ...action.users]
    }
    default: {
      return state
    }
  }
}

export default usersReducer
