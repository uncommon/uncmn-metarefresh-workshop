import { REQUEST_MESSAGES, RECEIVE_MESSAGES } from '../actions/messages'

function messagesReducer(state = [], action) {
  switch (action.type) {
    case REQUEST_MESSAGES: {
      return state
    }
    case RECEIVE_MESSAGES: {
      return [...state, ...action.messages]
    }
    default: {
      return state
    }
  }
}

export default messagesReducer
