import React, { Component } from 'react'
import Messages from './Messages'
import MessageInput from './MessageInput'
class Chat extends Component {
  render() {
    return (
      <div className="c-chatroom App__component">
        <header className="c-chatroom__header">
          <h1 className="c-chatroom__logo logo">Metatalk</h1>
          <div className="c-chatroom__header__gradient-overlay" />
        </header>
        <Messages />
        <MessageInput />
      </div>
    )
  }
}

export default Chat
