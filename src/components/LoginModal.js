import React, { Component } from 'react'
import { connect } from 'react-redux'
import sample from 'lodash.sample'

import firebaseFirestore from '../firebase'
import { login } from '../actions/me'
import { closeLoginModal } from '../actions/modals'

const uuidv4 = require('uuid/v4')

const imageUrls = [
  '/images/01.png',
  '/images/02.png',
  '/images/03.png',
  '/images/04.png',
  '/images/05.png',
  '/images/06.png',
  '/images/07.png',
  '/images/08.png',
  '/images/09.png',
  '/images/10.png',
  '/images/11.png',
  '/images/12.png',
  '/images/13.png',
  '/images/14.png',
  '/images/15.png',
  '/images/16.png',
  '/images/17.png',
  '/images/18.png',
  '/images/19.png',
  '/images/20.png'
]
class LoginModal extends Component {
  state = {
    inputValue: ''
  }

  onChange = event => {
    this.setState({
      inputValue: event.target.value
    })
  }

  onClick = () => {
    const user = {
      name: this.state.inputValue,
      id: uuidv4(),
      imageUrl: sample(imageUrls)
    }
    firebaseFirestore.collection('users').add(user)
    this.props.login(user)
    this.props.closeLoginModal()
  }

  onKeyPress = event => {
    if (event.key === 'Enter' && this.state.inputValue) {
      this.onClick()
    }
  }

  render() {
    const { inputValue } = this.state
    const { isOpen } = this.props
    return (
      isOpen && (
        <div className="c-enter">
          <div className="c-enter__content">
            <h1 className="h3 c-enter__title">Hi there!</h1>
            <p className="body c-enter__subtitle">
              Welcome to the National Gallery of Modern Art.
            </p>
            <input
              type="text"
              placeholder="Enter username..."
              value={inputValue}
              onChange={this.onChange}
              onKeyPress={this.onKeyPress}
              className="c-enter__input-name"
            />
            <button
              disabled={!inputValue}
              onClick={this.onClick}
              className="c-enter__button-start"
            >
              Start Talking
            </button>
          </div>
        </div>
      )
    )
  }
}

const mapDispatchToProps = {
  login,
  closeLoginModal
}

export default connect(
  null,
  mapDispatchToProps
)(LoginModal)
