import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ErrorImage } from './ErrorImage'

import { closeNsfwModal } from '../actions/modals'

class NsfwTextModal extends Component {
  render() {
    const { isOpen, closeNsfwModal } = this.props
    return (
      isOpen && (
        <div className="c-nsfw App__component modal">
          <div className="modal__content">
            <div className="c-nsfw__error-image">
              <ErrorImage />
            </div>
            <h1 className="h3 c-nsfw__title">That's not okay</h1>
            <p className="c-nsfw__description">
              You can’t use censored words like ‘to,’ ‘from,’ ‘and,’ etc.
            </p>
            <button onClick={closeNsfwModal}>
              <span>Pay the fine:</span>
              <span>INR 500</span>
            </button>
            <button onClick={closeNsfwModal} className="link-button">
              Sorry, Shaktiman
            </button>
          </div>
        </div>
      )
    )
  }
}

export default connect(
  null,
  { closeNsfwModal }
)(NsfwTextModal)
