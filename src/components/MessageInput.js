import React, { Component } from 'react'
import { connect } from 'react-redux'
import words from 'lodash.words'
import intersection from 'lodash.intersection'

import firebaseFirestore from '../firebase'

import { openNsfwModal } from '../actions/modals'
import { IconSend } from './Icons'

const uuidv4 = require('uuid/v4')

const blackList = ['to', 'from', 'and']
const isValueNsfw = (input, blackList) => {
  const sanitizedInput = words(input).map(word => word.toLowerCase())
  return intersection(sanitizedInput, blackList).length
}

class MessageInput extends Component {
  state = {
    inputValue: ''
  }

  onChange = event => {
    this.setState({
      inputValue: event.target.value
    })
    // Set isTyping to true
  }

  onClick = () => {
    if (isValueNsfw(this.state.inputValue, blackList)) {
      this.props.openNsfwModal()
    } else {
      firebaseFirestore.collection('messages').add({
        timestamp: Date.now(),
        senderId: this.props.me.id,
        content: this.state.inputValue,
        id: uuidv4()
      })
      this.setState({
        inputValue: ''
      })
    }
  }

  onKeyPress = event => {
    if (event.key === 'Enter' && this.state.inputValue) {
      this.onClick()
    }
  }

  render() {
    const { inputValue } = this.state
    return (
      <div className="c-enterMessage c-chatroom__enterMessage">
        {/* input-wrapper div is added for the purpose of styling */}
        <div className="c-enterMessage__input-wrapper">
          <div className="c-enterMessage__input-container">
            <input
              type="text"
              className="c-enterMessage__input"
              value={inputValue}
              placeholder="Write a message..."
              onChange={this.onChange}
              onKeyPress={this.onKeyPress}
            />
            <button
              disabled={!inputValue}
              className="c-enterMessage__button icon-button"
              onClick={this.onClick}
            >
              <IconSend />
            </button>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ me }) => ({
  me
})

const mapDispatchToProps = {
  openNsfwModal
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageInput)
