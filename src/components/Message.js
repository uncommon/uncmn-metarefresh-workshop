import React, { Component } from 'react'
import classNames from 'classnames'

import format from 'date-fns/format'

class Message extends Component {
  render() {
    const { sender, message, isSenderMe } = this.props
    const { name, imageUrl } = sender
    const { timestamp, content } = message
    const time = format(timestamp, 'hh:mm')
    const time_ampm = format(timestamp, 'a')

    return (
      <div
        className={classNames('c-message', {
          'c-message--current-user': isSenderMe,
          'c-message--external-user': !isSenderMe
        })}
      >
        <div className="c-message__content">
            <img className="c-message__img" src={imageUrl} alt="" />
            <div>
              <p className="c-message__metadata">
                <span className="h6 c-message__name">{name}</span>
                <span className="caption c-message__time">{time}</span>
                <span>&thinsp;</span>
                <span className="caption c-message__time-ampm">{time_ampm}</span>
              </p>
              <p className="c-message__text">{content}</p>
            </div>


        </div>
      </div>
    )
  }
}

export default Message
