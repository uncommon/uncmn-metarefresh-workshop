import React, { Component } from 'react'

export class IconSend extends Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
      >
        <g fill="none" fillRule="evenodd">
          <mask id="b" fill="#fff">
            <path
              id="a"
              d="M3.671 20.4l17.45-7.48a1 1 0 0 0 0-1.84L3.671 3.6a.993.993 0 0 0-1.39.91l-.01 4.61c0 .5.37.93.87.99L17.271 12l-14.13 1.88c-.5.07-.87.5-.87 1l.01 4.61c0 .71.73 1.2 1.39.91z"
            />
          </mask>
          <g fill="#000" mask="url(#b)">
            <path d="M0 0h24v24H0z" />
          </g>
        </g>
      </svg>
    )
  }
}
