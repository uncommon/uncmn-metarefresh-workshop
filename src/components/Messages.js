import React, { Component } from 'react'
import { connect } from 'react-redux'

import Message from './Message'

const defaultSender = {
  name: 'Anonymous',
  imageUrl: '/images/20.png'
}
class Messages extends Component {
  messagesEnd = React.createRef()

  componentDidMount() {
    this.scrollToBottom()
  }
  componentDidUpdate() {
    this.scrollToBottom()
  }
  scrollToBottom = () => {
    this.messagesEnd.current.scrollIntoView({ behavior: 'smooth' })
  }

  render() {
    const { messages, users, me } = this.props

    return (
      <div className="c-messages">
        {users.length &&
          messages.map(message => {
            const sender =
              users.find(user => user.id === message.senderId) || defaultSender
            const isSenderMe = me && sender.id === me.id
            return (
              <Message
                key={message.id}
                message={message}
                sender={sender}
                isSenderMe={isSenderMe}
              />
            )
          })}
        <div ref={this.messagesEnd} />
      </div>
    )
  }
}

const mapStateToProps = ({ messages, users, me }) => ({
  messages,
  users,
  me
})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Messages)
