import React, { Component } from 'react'
import { connect } from 'react-redux'

import LoginModal from './components/LoginModal'
import NsfwTextModal from './components/NsfwTextModal'
import Chat from './components/Chat'

import { receiveMessages } from './actions/messages'
import { receiveUsers } from './actions/users'

import './styles/App.scss'

import firebaseFirestore from './firebase'

class App extends Component {
  componentDidMount() {
    const { receiveMessages, receiveUsers } = this.props

    const messagesRef = firebaseFirestore
      .collection('messages')
      .orderBy('timestamp', 'asc')
    // .limit(10)
    messagesRef.onSnapshot(snapshot => {
      receiveMessages(snapshot.docChanges().map(change => change.doc.data()))
    })

    const usersRef = firebaseFirestore.collection('users')
    usersRef.onSnapshot(snapshot => {
      receiveUsers(snapshot.docChanges().map(change => change.doc.data()))
    })
  }

  render() {
    const { isLoginModalOpen, isNsfwModalOpen } = this.props
    return (
      <div className="App">
        <LoginModal isOpen={isLoginModalOpen} />
        <Chat />
        <NsfwTextModal isOpen={isNsfwModalOpen} />
      </div>
    )
  }
}

const mapStateToProps = ({ modals }) => ({
  ...modals
})

const mapDispatchToProps = {
  receiveMessages,
  receiveUsers
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
